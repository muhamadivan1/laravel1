<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function biodata(){
        return view('halaman.form');
    }
    public function kirim(Request $request){
        $nama = $request['nama'];
        $gender = $request['gender'];
        $nationality = $request['nationality'];
        $bio = $request['bio'];
        return view('halaman.home', compact('nama','gender','nationality','bio'));
    }
}
