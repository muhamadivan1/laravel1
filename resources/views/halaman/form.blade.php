<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="kirim" method="post">
        @csrf()
        <label>Name:</label><br><br>
        <input type="text" name="nama"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br>
        <input type="radio" name="gender">Other<br><br>
        <label>Nationality:</label><br><br>
        <select name="nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="Indonesia">England</option>
            <option value="Indonesia">France</option>
            <option value="Other">Other</option>
        </select><br><br>
        <!-- <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="indo">Bahasa Indonesia<br><br>
        <input type="checkbox" name="eng">English<br><br>
        <input type="checkbox" name="other">Other<br><br> -->
        <label>Bio</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>